// Fill out your copyright notice in the Description page of Project Settings.


#include "ShootingComponent.h"
#include "Projectile.h"
#include "TimerManager.h"
#include "Kismet/KismetMathLibrary.h"
#include "Components/ArrowComponent.h"
#include "GameFramework/ProjectileMovementComponent.h"
#include "Kismet/GameplayStatics.h"
#include "Buff.h"


// Sets default values for this component's properties
UShootingComponent::UShootingComponent()
{
	// Set this component to be initialized when the game starts, and to be ticked every frame.  You can turn these features
	// off to improve performance if you don't need them.
	PrimaryComponentTick.bCanEverTick = false;

	CanShoot = true;
}


void UShootingComponent::SetSpawnPoint(UArrowComponent* arrow)
{
	arrowComponent = arrow;
}

void UShootingComponent::SetProjectileReference(TSubclassOf<class AProjectile> projRef)
{
	ProjectileRef = projRef;
}

void UShootingComponent::SetDamage(int32 damage)
{
	this->Damage = damage;
}

void UShootingComponent::SetFireRate(float atkSpeed)
{
	FireRate = atkSpeed;
}

// Called when the game starts
void UShootingComponent::BeginPlay()
{
	Super::BeginPlay();

	// ...
	
}

void UShootingComponent::Shoot(AActor* target)
{
	if (!CanShoot) return;
	FRotator rot = UKismetMathLibrary::FindLookAtRotation(arrowComponent->GetComponentLocation(), target->GetActorLocation());
	FTransform transform = FTransform(FRotator(0), arrowComponent->GetComponentLocation());
	AProjectile* proj = GetWorld()->SpawnActorDeferred<AProjectile>(ProjectileRef, transform, nullptr, nullptr, ESpawnActorCollisionHandlingMethod::AlwaysSpawn);
	
	proj->SetTarget(target);
	proj->SetDamage(Damage);
	proj->GetProjectileComponent()->Velocity = (target->GetActorLocation() - arrowComponent->GetComponentLocation()).GetSafeNormal();
	proj->GetProjectileComponent()->InitialSpeed = 300000;
	proj->GetProjectileComponent()->MaxSpeed = 300000;
	
	if (Buff != nullptr) proj->Buff = Buff;

	UGameplayStatics::FinishSpawningActor(proj, transform);

	
	FTimerHandle handler;
	FTimerDelegate del;
	del.BindLambda([proj] {
		proj->Destroy();
	});
	FTimerManager x;
	GetWorld()->GetTimerManager().SetTimer(handler, del, 3.0f, false);
	CanShoot = false;
	ShootCooldown();
}

void UShootingComponent::SetBuff(TSubclassOf<UBuff> x)
{
	Buff = x;
}

void UShootingComponent::ShootCooldown() {
	FTimerHandle handler;
	FTimerDelegate del = FTimerDelegate::CreateUObject(this, &UShootingComponent::SetCanShoot, true);
	GetWorld()->GetTimerManager().SetTimer(handler, del, FireRate, false);
}

bool UShootingComponent::GetCanShoot()
{
	return CanShoot;
}

void UShootingComponent::SetCanShoot(bool value) {
	CanShoot = value;
}

