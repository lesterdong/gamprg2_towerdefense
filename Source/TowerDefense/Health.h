// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Components/ActorComponent.h"
#include "Health.generated.h"

DECLARE_DYNAMIC_MULTICAST_DELEGATE_OneParam(FDeathSignature, class UHealth*, health);
DECLARE_DYNAMIC_MULTICAST_DELEGATE(FNoParamDeathSignature);

UCLASS( ClassGroup=(Custom), meta=(BlueprintSpawnableComponent) )
class TOWERDEFENSE_API UHealth : public UActorComponent
{
	GENERATED_BODY()

public:	
	// Sets default values for this component's properties
	UHealth();
	UFUNCTION(BlueprintCallable)
		int32 GetCurrentHealth();
	UFUNCTION(BlueprintCallable)
		int32 GetMaxHealth();
	UFUNCTION(BlueprintCallable)
		void TakeDamage(int32 damage);
	UFUNCTION(BlueprintCallable)
		void Kill();

	void SetMaxHealth(int32 maxHealth);
	
	UPROPERTY(BlueprintAssignable)
		FDeathSignature DeathEvt;

	UPROPERTY(BlueprintAssignable)
		FNoParamDeathSignature DeathByBase;
	UPROPERTY(BlueprintAssignable)
		FNoParamDeathSignature NoParamDeathEvt;

	
protected:
	// Called when the game starts
	virtual void BeginPlay() override;
	
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
		int32 CurrentHealth;
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
		int32 MaxHealth;
	
public:	
	// Called every frame
	virtual void TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction) override;

		
};
