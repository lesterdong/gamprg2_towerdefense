// Fill out your copyright notice in the Description page of Project Settings.


#include "BuildableNodes.h"
#include "Components/ArrowComponent.h"
#include "Components/StaticMeshComponent.h"
#include "TowerBase.h"
#include "Kismet/GameplayStatics.h"

// Sets default values
ABuildableNodes::ABuildableNodes()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	StaticMesh = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("Static Mesh"));
	StaticMesh->SetupAttachment(RootComponent);
	
	ArrowComponent = CreateDefaultSubobject<UArrowComponent>(TEXT("Arrow"));
	ArrowComponent->SetupAttachment(StaticMesh);

	OnClicked.AddDynamic(this, &ABuildableNodes::OnSelected);
}

// Called when the game starts or when spawned
void ABuildableNodes::BeginPlay()
{
	Super::BeginPlay();
	
}

void ABuildableNodes::OnSelected(AActor* actor, FKey ButtonPressed)
{
	OnClickedEvt.Broadcast(this);
}

void ABuildableNodes::NotifyActorBeginCursorOver()
{
	if (BuiltTower != nullptr) {
		BuiltTower->RangeIndicator->SetVisibility(true);
	}
}

void ABuildableNodes::NotifyActorEndCursorOver()
{
	if (BuiltTower != nullptr) {
		BuiltTower->RangeIndicator->SetVisibility(false);
	}
}

// Called every frame
void ABuildableNodes::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

ATowerBase* ABuildableNodes::SetTower(TSubclassOf<ATowerBase> TowerToBuild)
{
	if (BuiltTower != nullptr) {
		BuiltTower->Destroy();
	}
	FTransform transform = FTransform(FRotator(0), GetActorLocation());
	ATowerBase* t = GetWorld()->SpawnActorDeferred<ATowerBase>(TowerToBuild, transform, nullptr, nullptr, ESpawnActorCollisionHandlingMethod::AlwaysSpawn);
	t->PlacedNode = this;

	BuiltTower = t;
	return Cast<ATowerBase>(UGameplayStatics::FinishSpawningActor(t, transform));

}

ATowerBase* ABuildableNodes::GetTower()
{
	return BuiltTower;
}

