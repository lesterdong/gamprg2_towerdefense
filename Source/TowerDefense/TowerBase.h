// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "TowerBase.generated.h"

UENUM()
enum TowerType {
	Shooting UMETA(DisplayName = "Shooting"),
	Buffer UMETA(DisplayName = "Buffer")

};

UCLASS()
class TOWERDEFENSE_API ATowerBase : public AActor
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	ATowerBase();
protected:

	
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly)
		class USphereComponent* RangeCollision;
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly)
		class USceneComponent* SceneComponent;

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly)
		TArray<class AEnemy*> EnemyInRange;

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly)
		class UStaticMeshComponent* StaticMesh;

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly)
		class UStaticMeshComponent* BodyMesh;

	UFUNCTION()
		void OnOverlapBegin(UPrimitiveComponent* OverlappedComp, AActor* OtherActor, UPrimitiveComponent* OtherComp, int32 OtherBodyIndex, bool bFromSweep, const FHitResult& SweepResult);
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly)
		class UArrowComponent* BulletSpawnPoint;
	UFUNCTION()
		 void OnOverlapExit(UPrimitiveComponent* OverlappedComp, AActor* OtherActor, UPrimitiveComponent* OtherComp, int32 OtherBodyIndex);


	UPROPERTY(VisibleAnywhere, BlueprintReadWrite)
		class UShootingComponent* ShootingComponent;

	UFUNCTION()
		void RemoveEnemyInList(class UHealth* health);

	UFUNCTION(BlueprintNativeEvent)
		void ChangeTarget();
	
	UPROPERTY(EditAnywhere, BlueprintReadOnly)
		TSubclassOf<class UBuff> Buff;

	UFUNCTION(BlueprintNativeEvent, BlueprintCallable)
		void ApplyBuff();


public:
	// Called every frame
	virtual void Tick(float DeltaTime) override;

	UFUNCTION()
		void SetBuildPreview(bool preview);
	UFUNCTION()
		void SetMaterial(class UMaterial* material);

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
		TEnumAsByte<TowerType> TowerType;

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly)
		class UStaticMeshComponent* RangeIndicator;

	virtual void NotifyActorBeginCursorOver() override;

	virtual void NotifyActorEndCursorOver() override;

	UPROPERTY(BlueprintReadOnly, VisibleAnywhere)
		bool BuildPreview = false;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
		class UBuff* AppliedBuff;

	UPROPERTY(EditAnywhere, BlueprintReadOnly)
		class UTowerData* TowerData;

	UPROPERTY(EditAnywhere, BlueprintReadOnly)
		class ABuildableNodes* PlacedNode;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
		class AEnemy* CurrentTarget;
};
