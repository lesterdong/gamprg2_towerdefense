// Fill out your copyright notice in the Description page of Project Settings.


#include "Projectile.h"
#include "Components/BoxComponent.h"
#include "Components/StaticMeshComponent.h"
#include "GameFramework/ProjectileMovementComponent.h"
#include "Enemy.h"
#include "Health.h"
#include "Buff.h"
#include "UObject/UObjectGlobals.h"
#include "Components/SceneComponent.h"

// Sets default values
AProjectile::AProjectile()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	SceneComponent = CreateDefaultSubobject<USceneComponent>(TEXT("Scene Component"));
	SetRootComponent(SceneComponent);

	StaticMesh = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("Static Mesh"));
	StaticMesh->SetupAttachment(RootComponent);
	StaticMesh->OnComponentBeginOverlap.AddDynamic(this, &AProjectile::OnOverlapBegin);

	ProjectileMovementComponent = CreateDefaultSubobject<UProjectileMovementComponent>(TEXT("Projectile Movement"));
}

// Called when the game starts or when spawned
void AProjectile::BeginPlay()
{
	Super::BeginPlay();
	
}

void AProjectile::OnOverlapBegin(UPrimitiveComponent* OverlappedComp, AActor* OtherActor, UPrimitiveComponent* OtherComp, int32 OtherBodyIndex, bool bFromSweep, const FHitResult& SweepResult)
{
	if (AEnemy* enemy = Cast<AEnemy>(OtherActor)) {
		enemy->GetHealth()->TakeDamage(Damage);
		Destroy();
		AddBuff(enemy);
	}
}

// Called every frame
void AProjectile::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);
}

UProjectileMovementComponent* AProjectile::GetProjectileComponent()
{
	return ProjectileMovementComponent;
}
void AProjectile::SetDamage(int32 damage) {
	Damage = damage;
}

void AProjectile::SetTarget(AActor* actor) {
	Target = actor;
}

AActor* AProjectile::GetActor()
{
	return Target;
}

void AProjectile::AddBuff(AActor* target)
{
	if (Buff == nullptr) return;

	if (target->IsPendingKill() == true) return;

	if (AEnemy* enemy = Cast<AEnemy>(target)) {
		for (UBuff* buffs : enemy->Buffs) {
			if (buffs->GetClass() == Buff) {
				buffs->RestartTimer();
				return;
			}
		}

		UBuff* buff = NewObject<UBuff>(target, Buff, "Buff");
		target->AddInstanceComponent(buff);
		buff->RegisterComponent();

		buff->EffectSubsided.AddDynamic(enemy, &AEnemy::RemoveBuffs);
		enemy->GetHealth()->NoParamDeathEvt.AddDynamic(buff, &UBuff::DestroyWithOwner);
	}
}