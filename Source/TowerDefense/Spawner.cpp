// Fill out your copyright notice in the Description page of Project Settings.


#include "Spawner.h"
#include "Components/StaticMeshComponent.h"
#include "Components/BoxComponent.h"
#include "Enemy.h"
#include "Kismet/KismetMathLibrary.h"
#include "Math/UnrealMathUtility.h"
#include "TDGameMode.h"
#include "Waves.h"
#include "Kismet/GameplayStatics.h"
#include "Health.h"
// Sets default values
ASpawner::ASpawner()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	StaticMesh = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("Static Mesh"));
	SetRootComponent(StaticMesh);

	SpawnPoint = CreateDefaultSubobject<UBoxComponent>(TEXT("Spawn Point"));
	SpawnPoint->SetupAttachment(StaticMesh);

}

// Called when the game starts or when spawned
void ASpawner::BeginPlay()
{
	Super::BeginPlay();
}

// Called every frame
void ASpawner::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

AEnemy* ASpawner::SpawnEnemy(TSubclassOf<AEnemy> enemyToSpawn, UWaves* WaveData) {

	FActorSpawnParameters spawnParameters;
	spawnParameters.SpawnCollisionHandlingOverride = ESpawnActorCollisionHandlingMethod::AdjustIfPossibleButAlwaysSpawn;
	
	FVector randomPosition = UKismetMathLibrary::RandomPointInBoundingBox(SpawnPoint->GetComponentLocation(), SpawnPoint->GetScaledBoxExtent());
	FTransform transform = FTransform(FRotator(0), randomPosition);
	AEnemy* enemy = GetWorld()->SpawnActorDeferred<AEnemy>(enemyToSpawn, transform, nullptr, nullptr, ESpawnActorCollisionHandlingMethod::AlwaysSpawn);
	enemy->SetWaypointPath(Waypoints[FMath::RandRange(0, Waypoints.Num() - 1)].WaypointPath);
	UHealth* enemyHealth = enemy->GetHealth();
	enemyHealth->SetMaxHealth(enemyHealth->GetMaxHealth() + (WaveData->CurrentWave * 0.1f));
	enemy->CoinsToDrop += WaveData->CurrentWave * 0.1f;

	return Cast<AEnemy>(UGameplayStatics::FinishSpawningActor(enemy, transform));
	//
	//
}

