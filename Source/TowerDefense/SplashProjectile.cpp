// Fill out your copyright notice in the Description page of Project Settings.


#include "SplashProjectile.h"
#include "Kismet/KismetSystemLibrary.h"
#include "Enemy.h"
#include "Health.h"
#include "Engine/Engine.h"

void ASplashProjectile::OnOverlapBegin(UPrimitiveComponent* OverlappedComp, AActor* OtherActor, UPrimitiveComponent* OtherComp, int32 OtherBodyIndex, bool bFromSweep, const FHitResult& SweepResult) {

	if (AEnemy* xd = Cast<AEnemy>(OtherActor)) {
		FVector explosionPosition = Target->GetActorLocation();
		TArray<TEnumAsByte<EObjectTypeQuery>> traceObjectTypes;
		traceObjectTypes.Add(UEngineTypes::ConvertToObjectType(ECollisionChannel::ECC_Pawn));
		TArray<AActor*> OverlappedActors;
		TArray<AActor*> IgnoredActors;
		UClass* seekClass = AEnemy::StaticClass();
		UKismetSystemLibrary::SphereOverlapActors(GetWorld(), explosionPosition, 5000.0f, traceObjectTypes, seekClass, IgnoredActors, OverlappedActors);

		for (AActor* actor : OverlappedActors) {

			if (AEnemy* enemy = Cast<AEnemy>(actor)) {

				if (enemy->MovementType == MovementType::Walking) {
					enemy->GetHealth()->TakeDamage(Damage);
					GEngine->AddOnScreenDebugMessage(-1, 5.f, FColor::Red, TEXT("WUW"));
				}
			}
		}

		Destroy();
	}

}