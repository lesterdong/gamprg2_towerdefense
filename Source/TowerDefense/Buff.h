// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Components/ActorComponent.h"
#include "Buff.generated.h"

DECLARE_DYNAMIC_MULTICAST_DELEGATE_OneParam(FRemovedSignature, UBuff*, Buff);

UCLASS( ClassGroup=(Custom), meta=(BlueprintSpawnableComponent), Blueprintable)
class TOWERDEFENSE_API UBuff : public UActorComponent
{
	GENERATED_BODY()

public:	
	// Sets default values for this component's properties
	UBuff();

	UPROPERTY(BlueprintAssignable)
		FRemovedSignature EffectSubsided;

	virtual void TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* tick) override;

	UFUNCTION()
		void DestroyWithOwner();

	UFUNCTION(BlueprintImplementableEvent, BlueprintCallable)
		void Effect();

	UFUNCTION(BlueprintImplementableEvent, BlueprintCallable)
		void AfterEffects();

	UFUNCTION(BlueprintCallable)
		void RestartTimer();

protected:
	// Called when the game starts
	virtual void BeginPlay() override;
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
		float Timer;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
		class UMaterial* MaterialEffect;

	UPROPERTY(BlueprintReadOnly, VisibleAnywhere)
		float RunningTimer;
	
};
