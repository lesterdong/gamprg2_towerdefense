// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "CombinedBaseCore.generated.h"

UCLASS()
class TOWERDEFENSE_API ACombinedBaseCore : public AActor
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	ACombinedBaseCore();

	UFUNCTION(BlueprintCallable)
		class UHealth* GetHealth();

	UFUNCTION(BlueprintCallable)
		TArray < class ABaseCore* > GetBaseCores();

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

	UPROPERTY(EditAnywhere)
		TArray<class ABaseCore*> BaseCores;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
		class UHealth* Health;



public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;
	

};
