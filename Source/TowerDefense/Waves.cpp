// Fill out your copyright notice in the Description page of Project Settings.


#include "Waves.h"

void UWaves::SetWave(int32 wave) {
	if (wave > MaxWave || wave < 0) return;
	CurrentWave = wave;
	WaveChange.Broadcast(CurrentWave);
	NumbersToSpawn = 6 + (CurrentWave * 3);
}

