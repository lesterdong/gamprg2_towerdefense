// Fill out your copyright notice in the Description page of Project Settings.


#include "BaseCore.h"
#include "Enemy.h"
#include "Components/StaticMeshComponent.h"
#include "Health.h"
#include "Components/SceneComponent.h"

// Sets default values
ABaseCore::ABaseCore()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	SceneComponent = CreateDefaultSubobject<USceneComponent>(TEXT("Scene Component"));
	SetRootComponent(SceneComponent);

	StaticMesh = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("Static Mesh"));
	StaticMesh->SetupAttachment(RootComponent);
	StaticMesh->OnComponentBeginOverlap.AddDynamic(this, &ABaseCore::OnOverlapBegin);
	
}

// Called when the game starts or when spawned
void ABaseCore::BeginPlay()
{
	Super::BeginPlay();
	//StaticMesh->OnComponentHit.AddDynamic(this, &ABaseCore::OnComponentHit);
}


// Called every frame
void ABaseCore::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

void ABaseCore::OnOverlapBegin(UPrimitiveComponent* OverlappedComp, AActor* OtherActor, UPrimitiveComponent* OtherComp, int32 OtherBodyIndex, bool bFromSweep, const FHitResult& SweepResult)
{
	if (AEnemy* enemy = Cast<AEnemy>(OtherActor)) {
		int32 x = 1;
		if (enemy->Type == EnemyType::Boss) x = 5;
		DamagedEvt.Broadcast(x);
		enemy->GetHealth()->Kill();
	}
}

//void ABaseCore::OnComponentHit(UPrimitiveComponent* HitComponent, AActor* OtherActor, UPrimitiveComponent* OtherComponent, FVector NormalImpulse, const FHitResult& Hit) {
//	if (AEnemy* enemy = Cast<AEnemy>(OtherActor)) {
//		DamagedEvt.Broadcast(1);
//		enemy->Destroy();
//	}
//}
//
