// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Engine/DataAsset.h"
#include "Waves.generated.h"

/**
 * 
 */
DECLARE_DYNAMIC_MULTICAST_DELEGATE_OneParam(FWaveSignature, int32, wave);

UCLASS()
class TOWERDEFENSE_API UWaves : public UDataAsset
{
	GENERATED_BODY()

public:
	UPROPERTY(BlueprintAssignable)
		FWaveSignature WaveChange;

	UFUNCTION(BlueprintCallable)
		void SetWave(int32 wave);

	UPROPERTY(EditAnywhere, BlueprintReadOnly)
		TArray<TSubclassOf<class AEnemy>> EnemyRefs;

	UPROPERTY(EditAnywhere, BlueprintReadOnly)
		TArray<TSubclassOf<class AEnemy>> BossRef;

	UPROPERTY(EditAnywhere, BlueprintReadOnly)
		int32 CurrentWave = 1;
	UPROPERTY(EditAnywhere, BlueprintReadOnly)
		int32 MaxWave = 20;
	UPROPERTY(EditAnywhere, BlueprintReadOnly)
		int32 NumbersToSpawn = 6;

protected:
	
	

	
};
