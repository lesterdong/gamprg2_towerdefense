// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Projectile.h"
#include "SplashProjectile.generated.h"

/**
 * 
 */
UCLASS()
class TOWERDEFENSE_API ASplashProjectile : public AProjectile
{
	GENERATED_BODY()
	

protected:
	virtual void OnOverlapBegin(UPrimitiveComponent* OverlappedComp, AActor* OtherActor, UPrimitiveComponent* OtherComp, int32 OtherBodyIndex, bool bFromSweep, const FHitResult& SweepResult) override;
};
