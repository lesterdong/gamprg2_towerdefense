// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Components/ActorComponent.h"
#include "ShootingComponent.generated.h"


UCLASS( ClassGroup=(Custom), meta=(BlueprintSpawnableComponent), Blueprintable )
class TOWERDEFENSE_API UShootingComponent : public UActorComponent
{
	GENERATED_BODY()

public:	
	// Sets default values for this component's properties
	UShootingComponent();
	void SetSpawnPoint(class UArrowComponent* arrow);
	void SetProjectileReference(TSubclassOf<class AProjectile> projRef);
	void SetDamage(int32 Damage);
	void SetFireRate(float atkSpeed);

protected:
	// Called when the game starts
	virtual void BeginPlay() override;
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
		float FireRate = 0.5f;
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
		TSubclassOf<class AProjectile> ProjectileRef;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
		class UArrowComponent* arrowComponent;

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly)
		bool CanShoot = true;

	UPROPERTY(EditAnywhere, BlueprintReadOnly)
		int32 Damage = 1;

	UPROPERTY(EditAnywhere, BlueprintReadOnly)
		TSubclassOf<class UBuff> Buff;

public:	
	UFUNCTION(BlueprintCallable)
		void Shoot(AActor* target);

	UFUNCTION()
		void SetBuff(TSubclassOf<class UBuff> x);

	UFUNCTION(BlueprintCallable)
		void SetCanShoot(bool value);

	UFUNCTION(BlueprintCallable)
		void ShootCooldown();

	bool GetCanShoot();

};
