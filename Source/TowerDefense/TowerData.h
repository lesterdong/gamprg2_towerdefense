// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Engine/DataAsset.h"
#include "TowerData.generated.h"

/**
 * 
 */
UCLASS()
class TOWERDEFENSE_API UTowerData : public UDataAsset
{
	GENERATED_BODY()

public:

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
		TSubclassOf<class ATowerBase> towerBaseRef;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
		UTowerData* TowerUpgradeData;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
		int32 Price;

	//UPROPERTY(EditAnywhere, BlueprintReadWrite)
	//	class UImage* Image;
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
		FString TowerName;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
		float FireRate;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
		int32  Damage;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
		TSubclassOf<class AProjectile> Proj;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
		TSubclassOf<class UBuff> Buff;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
		class UTexture2D* Image;
};
