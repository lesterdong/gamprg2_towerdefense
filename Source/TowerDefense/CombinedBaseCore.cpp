// Fill out your copyright notice in the Description page of Project Settings.


#include "CombinedBaseCore.h"
#include "Health.h"
#include "TDGameMode.h"
#include "BaseCore.h"

// Sets default values
ACombinedBaseCore::ACombinedBaseCore()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	Health = CreateDefaultSubobject<UHealth>(TEXT("Health"));
}

UHealth* ACombinedBaseCore::GetHealth()
{
	return Health;
}

// Called when the game starts or when spawned
void ACombinedBaseCore::BeginPlay()
{
	Super::BeginPlay();

	for (ABaseCore* core : BaseCores) {
		core->DamagedEvt.AddDynamic(Health, &UHealth::TakeDamage);
	}
	
}

// Called every frame
void ACombinedBaseCore::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

TArray<ABaseCore*> ACombinedBaseCore::GetBaseCores() {
	return BaseCores;
}
