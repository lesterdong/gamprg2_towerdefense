// Fill out your copyright notice in the Description page of Project Settings.


#include "BuildManager.h"
#include "BuildableNodes.h"
#include "TowerData.h"
#include "Kismet/GameplayStatics.h"
#include "TowerBase.h"

// Sets default values
ABuildManager::ABuildManager()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

}

void ABuildManager::OpenShop_Implementation(ABuildableNodes* buildableNode)
{
	SelectedNode = buildableNode;

}

void ABuildManager::CloseShop_Implementation(ABuildableNodes* buildableNode)
{
	SelectedNode = nullptr;
}

void ABuildManager::Sell_Implementation ()
{
	if (SelectedNode->BuiltTower == nullptr) return;

	Coins += (float)SelectedNode->BuiltTower->TowerData->Price / 2.0f;
	SelectedNode->BuiltTower->Destroy();
	SelectedNode->BuiltTower = nullptr;
}

void ABuildManager::ShowPreview(UTowerData* towerToBuy)
{
	FTransform transform = FTransform(FRotator(0), SelectedNode->GetActorLocation());
	ATowerBase* towerToBuild = GetWorld()->SpawnActorDeferred<ATowerBase>(towerToBuy->towerBaseRef, transform, nullptr, nullptr, ESpawnActorCollisionHandlingMethod::AlwaysSpawn);
	towerToBuild->SetBuildPreview(true);
	towerToBuild->SetMaterial(Coins >= towerToBuy->Price ? CanBuildMaterial : CantBuildMaterial);
	PreviewedObject = UGameplayStatics::FinishSpawningActor(towerToBuild, transform);
}

void ABuildManager::HidePreview()
{
	PreviewedObject->Destroy();
}

// Called when the game starts or when spawned
void ABuildManager::BeginPlay()
{
	Super::BeginPlay();
	
	if (BuildableArray.Num() <= 0) return;

	for (auto b : BuildableArray) {
		b->OnClickedEvt.AddDynamic(this, &ABuildManager::OpenShop);
		b->OnClicked.AddDynamic(this, &ABuildManager::OpenUpgrade);
	}
}

bool  ABuildManager::Buy_Implementation(UTowerData* towerToBuy)
{
	if (Coins < towerToBuy->Price) return false;

	ATowerBase* tower = SelectedNode->SetTower(towerToBuy->towerBaseRef);
	Coins -= towerToBuy->Price;
	tower->OnClicked.AddDynamic(this, &ABuildManager::OpenUpgrade);
	return true;
}

// Called every frame
void ABuildManager::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

void ABuildManager::AddCoins(int32 coinsToAdd)
{
	Coins += coinsToAdd;
}

int32 ABuildManager::GetCoins()
{
	return Coins;
}

void ABuildManager::UpgradeTower() {

	UTowerData* upgradeData = SelectedNode->BuiltTower->TowerData->TowerUpgradeData;

	if (upgradeData == nullptr) return;
	if (Coins < upgradeData->Price) return;

	Coins -= upgradeData->Price;
	ATowerBase* base = SelectedNode->SetTower(upgradeData->towerBaseRef);
	base->OnClicked.AddDynamic(this, &ABuildManager::OpenUpgrade);
}

