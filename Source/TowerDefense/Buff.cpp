// Fill out your copyright notice in the Description page of Project Settings.


#include "Buff.h"
#include "Materials/Material.h"
#include "Enemy.h"
#include "Components/StaticMeshComponent.h"

// Sets default values for this component's properties
UBuff::UBuff()
{
	// Set this component to be initialized when the game starts, and to be ticked every frame.  You can turn these features
	// off to improve performance if you don't need them.
	PrimaryComponentTick.bCanEverTick = true;
	PrimaryComponentTick.bStartWithTickEnabled = true;

	// ...
}


void UBuff::TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* tick)
{
	Super::TickComponent(DeltaTime, TickType, tick);
	RunningTimer -= DeltaTime;

	if (RunningTimer <= 0) {
		EffectSubsided.Broadcast(this);
		AfterEffects();
	}
}

void UBuff::DestroyWithOwner()
{
	DestroyComponent();
}

void UBuff::RestartTimer()
{
	RunningTimer = Timer;
}

// Called when the game starts
void UBuff::BeginPlay()
{
	Super::BeginPlay();
	
	RunningTimer = Timer;
	if (AEnemy* enemy = Cast<AEnemy>(GetOwner())) {
		MaterialEffect = Cast<UMaterial>(enemy->GetStaticMesh()->GetMaterial(0));
	}
	Effect();
	// ...
	
}

