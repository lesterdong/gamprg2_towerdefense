// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "BuildManager.generated.h"

UCLASS()
class TOWERDEFENSE_API ABuildManager : public AActor
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	ABuildManager();

	UFUNCTION(BlueprintNativeEvent, BlueprintCallable)
		void OpenShop(class ABuildableNodes* buildableNode);

	UFUNCTION(BlueprintNativeEvent, BlueprintCallable)
		void CloseShop(class ABuildableNodes* buildableNode);

	UFUNCTION(BlueprintNativeEvent, BlueprintCallable)
		bool Buy(class UTowerData* towerToBuy);

	UFUNCTION(BlueprintNativeEvent, BlueprintCallable)
		void Sell();

	UFUNCTION( BlueprintCallable)
		void ShowPreview(class UTowerData* towerToBuy);

	UFUNCTION(BlueprintCallable)
		void HidePreview();

	UPROPERTY(EditAnywhere, BlueprintReadOnly)
		TArray<class ABuildableNodes*> BuildableArray;

	UFUNCTION(BlueprintImplementableEvent, BlueprintCallable)
		void OpenUpgrade(AActor* actor, FKey key);

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

	UPROPERTY(VisibleAnywhere, BlueprintReadWrite)
		class ABuildableNodes* SelectedNode;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
		int32 Coins;

	UPROPERTY(EditAnywhere)
		class UMaterial* CanBuildMaterial;

	UPROPERTY(EditAnywhere)
		class UMaterial* CantBuildMaterial;

	class AActor* PreviewedObject;

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;

	UFUNCTION(BlueprintCallable)
		void AddCoins(int32 coinsToAdd);

	UFUNCTION(BlueprintCallable)
		int32 GetCoins();

	UFUNCTION(BlueprintCallable)
		void UpgradeTower();

};
