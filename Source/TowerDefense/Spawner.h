// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "Spawner.generated.h"

USTRUCT(BlueprintType)
struct FSetOfWaypointPath {
	GENERATED_BODY()

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
		TArray <class AWaypoint*> WaypointPath;
};


UCLASS()
class TOWERDEFENSE_API ASpawner : public AActor
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	ASpawner();

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly)
		class UStaticMeshComponent* StaticMesh;

	UPROPERTY(EditAnywhere, BlueprintReadOnly)
		class UBoxComponent* SpawnPoint;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
		TArray<FSetOfWaypointPath> Waypoints;



public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;

	UFUNCTION(BlueprintCallable)
		class AEnemy* SpawnEnemy(TSubclassOf<class AEnemy> enemyToSpawn, class UWaves* wave);

};
