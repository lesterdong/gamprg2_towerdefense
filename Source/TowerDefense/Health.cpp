// Fill out your copyright notice in the Description page of Project Settings.


#include "Health.h"

// Sets default values for this component's properties
UHealth::UHealth()
{
	// Set this component to be initialized when the game starts, and to be ticked every frame.  You can turn these features
	// off to improve performance if you don't need them.
	PrimaryComponentTick.bCanEverTick = false;

	// ...

}


int32 UHealth::GetCurrentHealth()
{
	return CurrentHealth;
}

int32 UHealth::GetMaxHealth()
{
	return MaxHealth;
}

void UHealth::TakeDamage(int32 damage)
{
	CurrentHealth -= damage;
	if (CurrentHealth <= 0) {
		CurrentHealth = 0;
		DeathEvt.Broadcast(this);
		NoParamDeathEvt.Broadcast();
	}

}

void UHealth::Kill()
{
	CurrentHealth = 0;
	//DeathEvt.Broadcast(this);
	DeathByBase.Broadcast();
	GetOwner()->Destroy();
}

void UHealth::SetMaxHealth(int32 maxHealth)
{
	MaxHealth = maxHealth;
	CurrentHealth = MaxHealth;
}

// Called when the game starts
void UHealth::BeginPlay()
{
	Super::BeginPlay();

	// ...
	
}


// Called every frame
void UHealth::TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction)
{
	Super::TickComponent(DeltaTime, TickType, ThisTickFunction);

	// ...
}

