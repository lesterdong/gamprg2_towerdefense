// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "BuildableNodes.generated.h"

DECLARE_DYNAMIC_MULTICAST_DELEGATE_OneParam(FOnClickedSignature, ABuildableNodes*, buildable);
UCLASS()
class TOWERDEFENSE_API ABuildableNodes : public AActor
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	ABuildableNodes();

	// Called every frame
	virtual void Tick(float DeltaTime) override;

	UFUNCTION(BlueprintCallable)
		class ATowerBase* SetTower(TSubclassOf<class ATowerBase> TowerToBuild);

	UFUNCTION(BlueprintCallable)
		class ATowerBase* GetTower();

	UPROPERTY(BlueprintAssignable)
		FOnClickedSignature OnClickedEvt;

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly)
		class ATowerBase* BuiltTower;

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly)
		class UStaticMeshComponent* StaticMesh;

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly)
		class UArrowComponent* ArrowComponent;


	UFUNCTION()
		void OnSelected(AActor* actor, FKey ButtonPressed);

	//UPROPERTY

	virtual void NotifyActorBeginCursorOver() override;

	virtual void NotifyActorEndCursorOver() override;
	

};
