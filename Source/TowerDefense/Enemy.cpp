// Fill out your copyright notice in the Description page of Project Settings.


#include "Enemy.h"
#include "Health.h"
#include "Components/StaticMeshComponent.h"
#include "Waypoint.h"
#include "Math/UnrealMathUtility.h"
#include "Kismet/KismetMathLibrary.h"
#include "Blueprint/AIBlueprintHelperLibrary.h"
#include "Components/SceneComponent.h"
#include "GameFramework/FloatingPawnMovement.h"
#include "Blueprint/AIAsyncTaskBlueprintProxy.h"
#include "BaseCore.h"
#include "Buff.h"

// Sets default values
AEnemy::AEnemy()
{
 	// Set this pawn to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;
	
	SceneComponent = CreateDefaultSubobject<USceneComponent>(TEXT("Scene Component"));
	SetRootComponent(SceneComponent);

	StaticMesh = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("Static Mesh"));
	StaticMesh->SetupAttachment(RootComponent);
	

	Health = CreateDefaultSubobject<UHealth>(TEXT("Health Component"));

	FloatingPawnMovement = CreateDefaultSubobject<UFloatingPawnMovement>(TEXT("Floating Movement"));
	CurrentWaypointIndex = 0;

	Speed = FloatingPawnMovement->MaxSpeed;

}

UStaticMeshComponent* AEnemy::GetStaticMesh()
{
	return StaticMesh;
}

// Called when the game starts or when spawned
void AEnemy::BeginPlay()
{
	Super::BeginPlay();
	SpawnDefaultController();
	GetController()->Possess(this);

	Health->DeathEvt.AddDynamic(this, &AEnemy::DestroyActorWithHealth);
	Health->NoParamDeathEvt.AddDynamic(this, &AEnemy::DestroyActor);
	Health->DeathByBase.AddDynamic(this, &AEnemy::DestroyActor);
}



// Called every frame
void AEnemy::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

	if (CurrentWaypointIndex >= WaypointPath.Num()) return;
	FRotator rotation = UKismetMathLibrary::FindLookAtRotation(GetActorLocation(), WaypointPath[CurrentWaypointIndex]->GetActorLocation());
	SetActorRotation(UKismetMathLibrary::RInterpTo(GetActorRotation(), rotation, DeltaTime, 20.0f));
	FloatingPawnMovement->AddInputVector(GetActorForwardVector(), false);


	if ((GetActorLocation() - WaypointPath[CurrentWaypointIndex]->GetActorLocation()).Size() <= 100.0f) {
		CurrentWaypointIndex++;
	}

	//FRotator rotation = 
	//SetActorRotation(rotation);
	//this->AddMovementInput(GetActorForwardVector(), 100.0f);

}

void AEnemy::SetWaypointPath(TArray<AWaypoint*> pathToFollow)
{
	/*if (this == nullptr) return;
	WaypointPath.AddUninitialized(pathToFollow.Num());
	for (int i = 0; i < pathToFollow.Num(); i++) {
		WaypointPath[i] = pathToFollow[i];
	}*/

	WaypointPath = pathToFollow;
}

void AEnemy::DestroyActor()
{
	Destroy();
}

void AEnemy::DestroyActorWithHealth(UHealth* health)
{
	health->GetOwner()->Destroy();
}

void AEnemy::RemoveBuffs(UBuff* buff)
{
	Buffs.Remove(buff);
}

UHealth* AEnemy::GetHealth() {
	return Health;
}

/*void AEnemy::OnComponentHit(UPrimitiveComponent* HitComponent, AActor* OtherActor, UPrimitiveComponent* OtherComponent, FVector NormalImpulse, const FHitResult& Hit)
{
	
}*/