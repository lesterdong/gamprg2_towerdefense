// Fill out your copyright notice in the Description page of Project Settings.


#include "TDGameMode.h"
#include "Waves.h"
#include "CombinedBaseCore.h"
#include "Spawner.h"
#include "Enemy.h"
#include "Health.h"
#include "Engine/Engine.h"
#include "BuildManager.h"

void ATDGameMode::SetTimer_Implementation(bool start)
{
	StartTimer = start;
	if (StartTimer)
		TimerBeforeStart = 5.0f;

}

ATDGameMode::ATDGameMode() {
	PrimaryActorTick.bCanEverTick = true;

}

void ATDGameMode::BeginPlay() {
	Super::BeginPlay();
	SetTimer(true);
	WaveData->SetWave(1);
	

	//Spawners[0]->SpawnEnemy(WaveData->EnemyRefs[0]);
	
	//FTimerDelegate::CreateUObject()
}

void ATDGameMode::Spawn(ASpawner* spawner, int32 toSpawn)
{
	
	if (toSpawn > (float)WaveData->NumbersToSpawn / 3.0f) return;
	AEnemy* enemy = spawner->SpawnEnemy(WaveData->EnemyRefs[FMath::RandRange(0, WaveData->EnemyRefs.Num() - 1)], WaveData);
	toSpawn++;
	enemy->GetHealth()->NoParamDeathEvt.AddDynamic(this, &ATDGameMode::ReduceSpawnedEnemiesCount);
	enemy->GetHealth()->DeathEvt.AddDynamic(this, &ATDGameMode::AddCoin);
	enemy->GetHealth()->DeathByBase.AddDynamic(this, &ATDGameMode::ReduceSpawnedEnemiesCount);

	FTimerHandle handle;
	FTimerDelegate del = FTimerDelegate::CreateUObject(this, &ATDGameMode::Spawn, spawner, toSpawn);
	GetWorldTimerManager().SetTimer(handle, del, 0.5f, false);

	
}

ACombinedBaseCore* ATDGameMode::GetCore() {
	return Core;
}

void ATDGameMode::SetCore(ACombinedBaseCore* core)
{
	Core = core;
}

void ATDGameMode::SetSpawners(ASpawner* spawner) {
	Spawners.Add(spawner);
}

void ATDGameMode::SpawnEnemies() {
	SpawnedEnemies = WaveData->NumbersToSpawn;
	for (ASpawner* spawner : Spawners) {
		int x = 1;
		Spawn(spawner, x);
	}

	if (WaveData->CurrentWave == 10) {
		AEnemy* enemyBoss = Spawners[FMath::RandRange(0, Spawners.Num() - 1)]->SpawnEnemy(WaveData->BossRef[FMath::RandRange(0, WaveData->BossRef.Num() - 1)], WaveData);
		enemyBoss->GetHealth()->NoParamDeathEvt.AddDynamic(this, &ATDGameMode::ReduceSpawnedEnemiesCount);
		enemyBoss->GetHealth()->DeathEvt.AddDynamic(this, &ATDGameMode::AddCoin);
		enemyBoss->GetHealth()->DeathByBase.AddDynamic(this, &ATDGameMode::ReduceSpawnedEnemiesCount);
		SpawnedEnemies++;
	}
	
}


void ATDGameMode::AddCoin(UHealth* health)
{
	AEnemy* enemy = Cast<AEnemy>(health->GetOwner());
	BuildManager->AddCoins(enemy->CoinsToDrop);
}

void ATDGameMode::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);
	if (!IsPlaying) return;
	if (StartTimer) {
		TimerBeforeStart -= DeltaTime;
		if (TimerBeforeStart <= 0) {
			SetTimer(false);
			SpawnEnemies();
		}
			
	}
	
}

void ATDGameMode::ReduceSpawnedEnemiesCount() {
	SpawnedEnemies--;
	if (SpawnedEnemies == 0 && WaveData->CurrentWave < 11) {
		WaveData->SetWave(WaveData->CurrentWave + 1);
		SetTimer(true);		
		BuildManager->AddCoins(300);
	}

	if (WaveData->CurrentWave > 10) {
		Win();
		IsPlaying = true;
	}
	Remove.Broadcast();

}