// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/GameModeBase.h"
#include "TDGameMode.generated.h"

DECLARE_DYNAMIC_MULTICAST_DELEGATE(FRemoveSignature);
/**
 * 
 */
UCLASS()
class TOWERDEFENSE_API ATDGameMode : public AGameModeBase
{
	GENERATED_BODY()

public:
	ATDGameMode();
	UFUNCTION(BlueprintCallable)
		class ACombinedBaseCore* GetCore();

	UFUNCTION()
		void SetCore(class ACombinedBaseCore* core);

	UFUNCTION()
		void SetSpawners(class ASpawner* spawner);

	UFUNCTION(BlueprintCallable)
		void SpawnEnemies();

	UFUNCTION(BlueprintNativeEvent, BlueprintCallable)
		void SetTimer(bool start);

	UFUNCTION()
		void AddCoin(class UHealth* health);

	UPROPERTY(BlueprintAssignable)
		FRemoveSignature Remove;

	UFUNCTION(BlueprintImplementableEvent)
		void Win();

	virtual void Tick(float DeltaTime) override;
protected:	
	virtual void BeginPlay() override;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
		TArray<class ASpawner*> Spawners;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
		class ACombinedBaseCore* Core;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
		class UWaves* WaveData;
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly)
	int32 SpawnedEnemies;

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly)
		float TimerBeforeStart;

	UPROPERTY(VisibleAnywhere, BlueprintReadWrite)
		class ABuildManager* BuildManager;

	UPROPERTY(VisibleAnywhere, BlueprintReadWrite)
		bool IsPlaying = true;


private:
	UFUNCTION()
	void Spawn(ASpawner* spawner, int32 toSpawn);

	UFUNCTION()
	void ReduceSpawnedEnemiesCount();

	bool StartTimer;


	
};
