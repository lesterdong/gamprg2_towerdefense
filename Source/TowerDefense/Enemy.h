// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Pawn.h"
#include "Enemy.generated.h"

UENUM()
enum EnemyType {
	Normal UMETA(DisplayName = "Normal"),
	Boss UMETA(DisplayName = "Boss")
};

UENUM()
enum MovementType {
	Walking UMETA(DisplayName = "Walking"),
	Flying UMETA(DisplayName = "Flying")
};

UCLASS()
class TOWERDEFENSE_API AEnemy : public APawn
{
	GENERATED_BODY()

public:
	// Sets default values for this pawn's properties
	AEnemy();

	UFUNCTION(BlueprintCallable)
		class UHealth* GetHealth();

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
		TEnumAsByte<EnemyType> Type;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
		TEnumAsByte<MovementType> MovementType;

	UFUNCTION()
		class UStaticMeshComponent* GetStaticMesh();


protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;
	
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly)
		class USceneComponent* SceneComponent;
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly)
		class UStaticMeshComponent* StaticMesh;
	UPROPERTY(VisibleAnywhere, BlueprintReadWrite)
		class UHealth* Health;

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly)
		class UFloatingPawnMovement* FloatingPawnMovement;

	UPROPERTY(VisibleAnywhere)
		TArray<class AWaypoint*> WaypointPath;
	
	int32 CurrentWaypointIndex;	


		//void OnComponentHit(UPrimitiveComponent* HitComponent, AActor* OtherActor, UPrimitiveComponent* OtherComponent, FVector NormalImpulse, const FHitResult& Hit);
		
	//UPROPERTY(EditAnywhere, BlueprintReadWrite)

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;

	void SetWaypointPath(TArray<class AWaypoint*> pathToFollow);
		///*int32 size = pathToFollow.Num();
		//WaypointPath.AddUninitialized(size);
		//for (int i = 0; i < size; i++) {
		//	WaypointPath[i] = pathToFollow[i];
		//}*/
		//if (pathToFollow.Num() < 1) return;
		//

	UFUNCTION()
		void DestroyActor();
	
	UFUNCTION()
		void DestroyActorWithHealth(class UHealth* x);

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly)
		int32 CoinsToDrop = 5;

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly)
		float Speed;

	UPROPERTY(VisibleAnywhere, BlueprintReadWrite)
		TArray<class UBuff*> Buffs;

	UFUNCTION()
		void RemoveBuffs(class UBuff* buff);
};
