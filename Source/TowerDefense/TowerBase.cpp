// Fill out your copyright notice in the Description page of Project Settings.


#include "TowerBase.h"
#include "Components/StaticMeshComponent.h"
#include "Enemy.h"
#include "Components/SphereComponent.h"
#include "Components/ArrowComponent.h"
#include "Projectile.h"
#include "Kismet/KismetMathLibrary.h"
#include "ShootingComponent.h"
#include "Health.h"
#include "Components/SceneComponent.h"
#include "Materials/Material.h"
#include "Buff.h"
#include "TowerData.h"

// Sets default values
ATowerBase::ATowerBase()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	SceneComponent = CreateDefaultSubobject<USceneComponent>(TEXT("Scene Component"));
	SetRootComponent(SceneComponent);

	StaticMesh = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("Static Mesh"));
	StaticMesh->SetupAttachment(RootComponent);
	//StaticMesh->SetCollisionEnabled(ECollisionEnabled::NoCollision);

	BodyMesh = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("Body Mesh"));
	BodyMesh->SetupAttachment(RootComponent);
	//BodyMesh->SetCollisionEnabled(ECollisionEnabled::NoCollision);

	RangeCollision = CreateDefaultSubobject<USphereComponent>(TEXT("Range Collision"));
	RangeCollision->SetupAttachment(RootComponent);
	RangeCollision->OnComponentBeginOverlap.AddDynamic(this, &ATowerBase::OnOverlapBegin);
	RangeCollision->OnComponentEndOverlap.AddDynamic(this, &ATowerBase::OnOverlapExit);

	BulletSpawnPoint = CreateDefaultSubobject<UArrowComponent>(TEXT("Arrow"));
	BulletSpawnPoint->SetupAttachment(RootComponent);

	RangeIndicator = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("Range Indicator"));
	RangeIndicator->SetupAttachment(RootComponent);
	RangeIndicator->SetCollisionEnabled(ECollisionEnabled::NoCollision);

	ShootingComponent = CreateDefaultSubobject<UShootingComponent>(TEXT("ShootingComponent"));
}

// Called when the game starts or when spawned
void ATowerBase::BeginPlay()
{
	Super::BeginPlay();
	
	Buff = TowerData->Buff;

	ShootingComponent->SetSpawnPoint(BulletSpawnPoint);
	ShootingComponent->SetBuff(Buff);
	ShootingComponent->SetBuff(Buff);
	ShootingComponent->SetProjectileReference(TowerData->Proj);
	ShootingComponent->SetDamage(TowerData->Damage);
	ShootingComponent->SetFireRate(TowerData->FireRate);

	if (!BuildPreview) RangeIndicator->SetVisibility(false);
}

void ATowerBase::OnOverlapBegin(UPrimitiveComponent* OverlappedComp, AActor* OtherActor, UPrimitiveComponent* OtherComp, int32 OtherBodyIndex, bool bFromSweep, const FHitResult& SweepResult) {
	if (AEnemy* enemy = Cast<AEnemy>(OtherActor)) {
		EnemyInRange.Add(enemy);
		enemy->GetHealth()->DeathEvt.AddDynamic(this, &ATowerBase::RemoveEnemyInList);
	}
}
	
void ATowerBase::OnOverlapExit(UPrimitiveComponent* OverlappedComp, AActor* OtherActor, UPrimitiveComponent* OtherComp, int32 OtherBodyIndex) {
	if (AEnemy* enemy = Cast<AEnemy>(OtherActor)) {
		EnemyInRange.Remove(enemy);
		enemy->GetHealth()->DeathEvt.RemoveDynamic(this, &ATowerBase::RemoveEnemyInList);
	}
}

void ATowerBase::ApplyBuff_Implementation()
{
	ShootingComponent->SetCanShoot(false);
	ShootingComponent->ShootCooldown();
}

// Called every frame
void ATowerBase::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

	if (BuildPreview) return;

	if (TowerType == TowerType::Shooting) {
		if (EnemyInRange.Num() < 1) return;
		ChangeTarget();
		FRotator rotation = UKismetMathLibrary::FindLookAtRotation(GetActorLocation(), EnemyInRange[0]->GetActorLocation());
		SetActorRotation(rotation);
		ShootingComponent->Shoot(CurrentTarget);
	}

	else if (TowerType == TowerType::Buffer) {
		if (ShootingComponent->GetCanShoot()) {
			ApplyBuff();
		}
	}

}

void ATowerBase::SetBuildPreview(bool preview)
{
	BuildPreview = preview;
}

void ATowerBase::SetMaterial(UMaterial* material)
{
	StaticMesh->SetMaterial(0, material);
	BodyMesh->SetMaterial(0, material);
}

void ATowerBase::NotifyActorBeginCursorOver()
{
	RangeIndicator->SetVisibility(true);
}

void ATowerBase::NotifyActorEndCursorOver()
{
	RangeIndicator->SetVisibility(false);
}

void ATowerBase::RemoveEnemyInList(UHealth* health) {
	EnemyInRange.Remove(Cast<AEnemy>(health->GetOwner()));
}

void ATowerBase::ChangeTarget_Implementation()
{
	CurrentTarget = EnemyInRange[0];
}
